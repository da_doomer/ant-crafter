"""Sampling of recipe books."""
from pathlib import Path
from ant_crafter.mujoco.map import Recipe
from ant_crafter.mujoco.simulation import TRASH_ITEM_NAME
from ant_crafter.map_parser import jsonify_recipe
from typing import TypeVar
import argparse
import json
import random


object_dir = Path(__file__).parent.parent/"lib"/"object_sim"


RecipeBook = list[Recipe]


def get_available_items() -> list[str]:
    item_dirs = list(object_dir.glob("*"))
    forbidden_items = [
        "cubemiddle", "cubesmall", "cubelarge",
        "toruslarge", "torussmall",
        "spheresmall","spherelarge",
        "pyramidlarge", "pyramidsmall",
        "cylinderlarge", "cylindersmall",
        # "hand", "doorknob",  # These trip-up the LLM that writes code
        "flute",
        "toothbrush",
        TRASH_ITEM_NAME,
        "table",
        "human",
        "scissors",
        "phone",
        "README.md",
        "LICENSE",
        ".git",
        "common.xml",
        "objects.png",
    ]
    item_names = [
        item_dir.name
        for item_dir in item_dirs
    ]
    item_names = [
        item_name
        for item_name in item_names
        if item_name not in forbidden_items
    ]
    return sorted(item_names)


X = TypeVar("X")
def get_shuffled(x: list[X], seed: str) -> list[X]:
    new_x = list(x)
    _random = random.Random(seed)
    _random.shuffle(new_x)
    return new_x


def get_recipe_book(
    seed: str,
) -> RecipeBook:
    """Sample a random recipe book using the given seed."""
    _random = random.Random(seed)
    item_names = get_available_items()

    recipes = list()

    # We will create a binary tree with items
    remaining_items = get_shuffled(
        item_names,
        seed=str(_random.random()),
    )

    while len(remaining_items) > 2:
        # The first half of the remaining items are leaves, the rest
        # are outputs
        n = len(remaining_items)
        non_leaves = list(remaining_items[:n//2])
        leaves = list(remaining_items[n//2:])
        outputs = list(non_leaves)

        while len(leaves) > 1:
            # We know the length of leaves is a positive even number
            output = outputs.pop()
            ingredients = [
                leaves.pop()
                for _ in range(2)
            ]
            recipe = Recipe(
                input_items=ingredients,
                output_item=output,
            )
            recipes.append(recipe)

        # We take the leaves out of the remaining items
        remaining_items = list(non_leaves)

    return recipes


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description='Print a random recipe book JSON.'
    )
    parser.add_argument(
        '--seed',
        type=str,
        required=True,
        help='Sampling seed.',
    )
    args = parser.parse_args()
    recipe_book = get_recipe_book(args.seed)
    recipe_book_json = [
        jsonify_recipe(recipe)
        for recipe in recipe_book
    ]
    recipe_book_str = json.dumps(recipe_book_json, indent=2)
    print(recipe_book_str)
