"""Predicate language."""
from dataclasses import dataclass


@dataclass(frozen=True)
class RobotAtLocation:
    target_x: float
    target_y: float


@dataclass(frozen=True)
class ItemAtLocation:
    item_name: str
    target_x: float
    target_y: float


@dataclass(frozen=True)
class RobotRollAxisLessThan:
    radians: float


@dataclass(frozen=True)
class RobotPitchAxisLessThan:
    radians: float


@dataclass(frozen=True)
class RobotYawAxisLessThan:
    radians: float


@dataclass(frozen=True)
class RobotContactWithItem:
    item_name: str


@dataclass(frozen=True)
class ItemInContactWithItem:
    item1_name: str
    item2_name: str


@dataclass(frozen=True)
class Eventually:
    """Time indices are global."""
    min_t: int
    max_t: int
    predicate: "Predicate"


@dataclass(frozen=True)
class Always:
    min_t: int
    max_t: int
    predicate: "Predicate"


@dataclass(init=False, frozen=True)
class And:
    predicate_left: "Predicate"
    predicate_right: "Predicate"

    def __init__(self, *args: "Predicate"):
        if len(args) < 2:
            raise ValueError("At least two arguments required!")
        first, *rest = args
        object.__setattr__(self, "predicate_left", first)
        if len(rest) == 1:
            object.__setattr__(self, "predicate_right", rest[0])
        else:
            object.__setattr__(self, "predicate_right", And(*rest))


@dataclass(init=False, frozen=True)
class Or:
    predicate_left: "Predicate"
    predicate_right: "Predicate"

    def __init__(self, *args: "Predicate"):
        if len(args) < 2:
            raise ValueError("At least two arguments required!")
        first, *rest = args
        object.__setattr__(self, "predicate_left", first)
        if len(rest) == 1:
            object.__setattr__(self, "predicate_right", rest[0])
        else:
            object.__setattr__(self, "predicate_right", Or(*rest))


@dataclass(frozen=True)
class Not:
    predicate: "Predicate"


Predicate = (
    RobotAtLocation |
    RobotYawAxisLessThan |
    RobotPitchAxisLessThan |
    RobotRollAxisLessThan |
    ItemAtLocation |
    RobotContactWithItem |
    ItemInContactWithItem |
    Eventually |
    And |
    Or |
    Not |
    Always
)
