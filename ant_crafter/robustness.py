"""Robustness values for predicates.

A predicate in some context is said to be true if and only if its corresponding
robustness value is greater than zero."""
from ant_crafter.predicates import Predicate
from ant_crafter.predicates import RobotContactWithItem
from ant_crafter.predicates import RobotYawAxisLessThan
from ant_crafter.predicates import RobotPitchAxisLessThan
from ant_crafter.predicates import RobotRollAxisLessThan
from ant_crafter.predicates import ItemInContactWithItem
from ant_crafter.predicates import And
from ant_crafter.predicates import Or
from ant_crafter.predicates import Not
from ant_crafter.predicates import RobotAtLocation
from ant_crafter.predicates import ItemAtLocation
from ant_crafter.predicates import Eventually
from ant_crafter.predicates import Always
from ant_crafter.predicates import Always
from ant_crafter.mujoco.map import AbstractMapState, MapSpec
from ant_crafter.mujoco.simulation import BUTTON_RADIUS
from ant_crafter.mujoco.simulation import SimulationState
from ant_crafter.mujoco.simulation import get_simulation_instance
from ant_crafter.mujoco.simulation import get_abstract_map_state
from itertools import product
import numpy as np


def get_robustness_value(
        plan: list[list[float]],
        map_spec: MapSpec,
        simulation_state: SimulationState,
        predicate: Predicate,
        t: int,
        sub_step_s: float,
) -> float:
    """Return the robustness value of the given Signal Temporal Logic
    predicate with respect to the simulation induced by the given plan
    and simulation spec."""
    # Get simulation instance
    simulation = get_simulation_instance(
        map_spec,
        animation_fps=None,
        sub_step_s=sub_step_s,
    )

    # Set simulation state to the given state
    simulation.state = simulation_state
    state_sequence = list[AbstractMapState]()
    for action in plan:
        simulation.step(np.array(action))
        abstract_state = get_abstract_map_state(simulation)
        state_sequence.append(abstract_state)

    return _get_robustness_value(
        signal=state_sequence,
        predicate=predicate,
        t=t,
    )


def get_distance(p1: tuple[float, float], p2: tuple[float, float]) -> float:
    """Get the distance between the two given points."""
    distance = np.linalg.norm(
        np.array(p1) - np.array(p2)
    ).item()
    return distance


def less_than(v1: float, v2: float) -> float:
    """Smooth version of less than operator.

    That is `less_than(v1, v2) > 0` if and only if `v1 < v2 == True`."""
    return v2 - v1


def _get_robustness_value(
        signal: list[AbstractMapState],
        predicate: Predicate,
        t: int,
) -> float:
    """Return the robustness value for the given predicate and state signal."""
    match predicate:
        case RobotAtLocation(target_x, target_y):
            min_distance = BUTTON_RADIUS*0.5
            position = (
                signal[t].robot_position.x,
                signal[t].robot_position.y,
            )
            target_position = (target_x, target_y)
            distance = get_distance(position, target_position)
            robustness_value = min_distance - distance
            return robustness_value

        case ItemAtLocation(item_name, target_x, target_y):
            # There might be more than one item with the given name,
            # so this is effectively a disjunction over all such items
            # being at the given target position. If there are no such
            # items, the robustness value is an arbitrary positive constant
            # (negative one).

            # Identify all distances to the target position for the items
            # with the given name
            item_positions = [
                (item.position.x, item.position.y)
                for item in signal[t].items
                if item.item_name == item_name
            ]
            target_position = (target_x, target_y)
            distances = [
                get_distance(p1, target_position)
                for p1 in item_positions
            ]
            if len(distances) == 0:
                return -1.0
            min_distance = BUTTON_RADIUS*0.5
            robustness_value = min_distance - min(distances)
            return robustness_value

        case RobotPitchAxisLessThan(radians=max_radians):
            radians = abs(signal[t].robot_pitch_radians)
            robustness_value = less_than(radians, max_radians)
            return robustness_value

        case RobotRollAxisLessThan(radians=max_radians):
            radians = abs(signal[t].robot_roll_radians)
            robustness_value = less_than(radians, max_radians)
            return robustness_value

        case RobotYawAxisLessThan(radians=max_radians):
            radians = abs(signal[t].robot_yaw_radians)
            robustness_value = less_than(radians, max_radians)
            return robustness_value

        case RobotContactWithItem(item_name):
            # There might be more than one item with the given name,
            # so this is effectively a disjunction over all such items
            # making contact with the robot. If there are no such
            # items, the robustness value is an arbitrary positive constant
            # (-1).

            # Check if there are collisions between the robot and any
            # item with the given name
            item_contacts = signal[t].robot_item_contact_names
            if item_name in item_contacts:
                return 1.0

            # Otherwise, the robot should come approach any such object
            # Identify all positions for the items with the given name
            item_positions = [
                (item.position.x, item.position.y)
                for item in signal[t].items
                if item.item_name == item_name
            ]
            if len(item_positions) == 0:
                return -1.0

            # Get the minimum distance to an item with the given name
            robot_position = (
                signal[t].robot_position.x,
                signal[t].robot_position.y,
            )
            distances = [
                get_distance(target_position, robot_position)
                for target_position in item_positions
            ]
            min_distance = BUTTON_RADIUS*0.2
            robustness_value = min_distance - min(distances)
            return robustness_value

        case ItemInContactWithItem(item1_name, item2_name):
            # There might be more than one pair of items with the given names,
            # so this is effectively a disjunction over all such pairs making
            # contact. If there are no such pairs, the robustness value is an
            # arbitrary positive constant (-1).

            for (ci1, ci2) in signal[t].item_item_contact_pairs:
                if sorted((ci1, ci2)) == sorted((item1_name, item2_name)):
                    return 1.0

            # Identify all pairs of items that match the names
            pairs = list[tuple[int, int]]()
            idxs = list(range(len(signal[t].items)))
            for (i, j) in product(idxs, idxs):
                if i == j:
                    continue
                if signal[t].items[i].item_name != item1_name:
                    continue
                if signal[t].items[j].item_name != item2_name:
                    continue
                pairs.append((i, j))

            # Compute the distance between each pair
            distances = list[float]()
            for (i, j) in pairs:
                p1 = (
                    signal[t].items[i].position.x,
                    signal[t].items[i].position.y,
                )
                p2 = (
                    signal[t].items[j].position.x,
                    signal[t].items[j].position.y,
                )
                distance = get_distance(p1, p2)
                distances.append(distance)

            if len(distances) == 0:
                return -1.0

            robustness_value = -min(distances)
            return robustness_value

        case Eventually(min_t=min_t, max_t=max_t, predicate=predicate):
            values = [
                _get_robustness_value(
                    signal=signal,
                    predicate=predicate,
                    t=tp
                )
                for tp in range(t+min_t, min(t+max_t+1, len(signal)))
            ]
            return max(values)

        case Always(min_t=min_t, max_t=max_t, predicate=predicate):
            # I know Always(p) = not Eventually (not P), but I do this
            # equivalent computation instead
            values = [
                _get_robustness_value(
                    signal=signal,
                    predicate=predicate,
                    t=tp
                )
                for tp in range(t+min_t, min(t+max_t+1, len(signal)))
            ]
            return min(values)

        case And(
                predicate_left=predicate_left,
                predicate_right=predicate_right,
        ):
            r1 = _get_robustness_value(
                signal=signal,
                predicate=predicate_left,
                t=t,
            )
            r2 = _get_robustness_value(
                signal=signal,
                predicate=predicate_right,
                t=t,
            )
            if r1 < 0.0 and r2 < 0.0:
                # A little boost to distinguish where one of the terms
                # has already been solved
                return r1 + r2 - 1
            return min(r1, r2)

        case Or(
                predicate_left=predicate_left,
                predicate_right=predicate_right,
        ):
            r1 = _get_robustness_value(
                signal=signal,
                predicate=predicate_left,
                t=t,
            )
            r2 = _get_robustness_value(
                signal=signal,
                predicate=predicate_right,
                t=t,
            )
            return max(r1, r2)

        case Not(predicate=predicate):
            r = _get_robustness_value(
                signal=signal,
                predicate=predicate,
                t=t,
            )
            return -r

    raise ValueError("Predicate type not recognized!")
