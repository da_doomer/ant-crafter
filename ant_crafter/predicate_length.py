"""Get the number of timesteps covered by a predicate."""
import ant_crafter.predicates as predicates


def get_max_plan_length(predicate: predicates.Predicate, min_length: int) -> int:
    match predicate:
        case predicates.And(predicate_left=predicate_left, predicate_right=predicate_right):
            max1 = get_max_plan_length(predicate_left, min_length)
            max2 = get_max_plan_length(predicate_right, min_length)
            return max(max1, max2)
        case predicates.Eventually(min_t=_, max_t=new_max_t, predicate=ipredicate):
            max1 = get_max_plan_length(ipredicate, min_length)
            return max(max1, new_max_t, min_length)
        case _:
            return min_length
    raise ValueError("Predicate not recognized!")
