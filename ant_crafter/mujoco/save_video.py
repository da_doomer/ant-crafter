from pathlib import Path
import tempfile
from PIL import Image
import numpy as np
import ffmpeg


def save_video(
    frames: list[np.ndarray],
    output_path: Path,
    fps: float,
) -> None:
    """Plot an animation formed by the given list of frames (given as PNG
    arrays)."""
    with tempfile.TemporaryDirectory() as tdir:
        frame_dir = Path(tdir)
        for i, frame in enumerate(frames):
            frame_path = frame_dir/(f"{i}.png").rjust(10, "0")
            im = Image.fromarray(frame)
            im.save(frame_path)
        (
            ffmpeg
            .input(frame_dir/"*.png", pattern_type="glob", framerate=fps)
            .output(str(output_path))
            .run(quiet=True)
        )
