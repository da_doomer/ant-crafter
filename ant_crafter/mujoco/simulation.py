"""Mujoco simulation definition for the Ant environment.

Robot definitions are from DeepMind's PyMJCF tutorial:
https://colab.research.google.com/github/deepmind/dm_control/blob/main/tutorial.ipynb#scrollTo=UAMItwu8e1WR
"""
from dataclasses import dataclass
from typing import Union
from ant_crafter.mujoco.map import AbstractMapState
from ant_crafter.mujoco.map import DiningTableSpec
from ant_crafter.mujoco.map import TrashcanSpec
from ant_crafter.mujoco.map import CraftingTableSpec
from ant_crafter.mujoco.map import ItemSpec
from ant_crafter.mujoco.map import DoorSpec
from ant_crafter.mujoco.map import MapSpec
from ant_crafter.mujoco.map import Position
from ant_crafter.mujoco.map import WallSpec
from ant_crafter.mujoco.map import FryingPanSpec
from ant_crafter.mujoco.map import PlateDispenserSpec
from ant_crafter.mujoco.map import Recipe
from ant_crafter.mujoco.map import FryingRecipe
from ant_crafter.mujoco.robot import make_robot
from ant_crafter.mujoco.arena import make_arena
from ant_crafter.mujoco.objects import Cylinder
from ant_crafter.mujoco.objects import Box
from ant_crafter.mujoco.objects import make_cylinder
from ant_crafter.mujoco.objects import make_box
from ant_crafter.mujoco.objects import get_wall_color
from ant_crafter.mujoco.objects import get_frying_pan_color
from ant_crafter.mujoco.objects import get_door_color
from ant_crafter.mujoco.objects import get_crafting_table_color
from ant_crafter.mujoco.objects import get_trashcan_color
from ant_crafter.mujoco.objects import get_dining_table_color
from ant_crafter.mujoco.objects import get_item
from ant_crafter.mujoco.save_video import save_video
from ant_crafter.mujoco.euler_angles import get_euler_angles
from dm_control import mjcf
from dm_control.mujoco import engine
import numpy as np
from pathlib import Path
from typing import Any

from numpy.typing import NDArray


BASE_LENGTH = 0.4
BODY_RADIUS = BASE_LENGTH/5
ITEM_DEFAULT_HEIGHT = BASE_LENGTH
BUTTON_RADIUS = BASE_LENGTH/4
BODY_SIZE = (BODY_RADIUS, BODY_RADIUS, BODY_RADIUS / 2)
CRAFTING_TABLE_SIZE = (BASE_LENGTH/4, BASE_LENGTH/4, BASE_LENGTH/20)
MAP_BOUNDARY_HEIGHT = BASE_LENGTH/5
MAP_BOUNDARY_WIDTH = BASE_LENGTH/5
CAMERA_DISTANCE = BASE_LENGTH*6
CAMERA_AZIMUTH_DEGREES = 90.0
CAMERA_ELEVATION_DEGREES = -60.0
CAMERA_ROTATION_SPEED_DEG_S = 90.0/3
TRASH_ITEM_NAME = "cubelarge"


@dataclass
class TaskState:
    crafted_items_log: list[str]
    opened_doors: list[int]
    activated_buttons: list[int]
    spawned_item_geom_names: list[str]
    nonspawned_item_geom_names: list[str]
    trashcan_geom_names: list[str]
    dining_table_geom_names: list[str]
    frying_pan_geom_names: list[str]


@dataclass
class SimulationState:
    mujoco_state: list[float]
    task_state: TaskState


def get_spawnable_item_specs(
    recipes: list[Recipe],
    item_specs: list[ItemSpec],
    frying_recipes: list[FryingRecipe],
    start_position: Position = Position(x=100, y=0),
    spawn_gap: float = BASE_LENGTH,
) -> list[ItemSpec]:
    """Return a list of new items that could be spawned with the given
    recipes.

    The items are positioned far away from the robot so that they are
    effectively not yet spawned into the simulation.

    The items are positioned starting at the specified starting position,
    increasing on first coordinate for each item.

    This function thus makes several assumptions:
    - The map is small so that the items are placed outside.
    - There is a sufficiently large (e.g., infinite) plane so that the
      items don't fall indefinitely, which might (?) break the simulation.
    - The items are less than `spawn_gap` in size in horizontal dimension.

    **Assumptions:**
    - every item is present in at most one recipe
    - at most one copy of an item is needed in every recipe.
    """
    spawnable_item_names = set[str]()
    initial_item_names = set([
        spec.item_name
        for spec in item_specs
    ])
    # Convert frying recipes to recipes for ease of processing
    _frying_recipes = [
        Recipe(input_items=[r.input_item], output_item=r.output_item)
        for r in frying_recipes
    ]
    all_recipes = recipes + _frying_recipes
    while True:
        prev_spawnable_item_names = set(spawnable_item_names)

        for recipe in all_recipes:
            # Check if all prerequisites can be crafted
            available_items = spawnable_item_names | initial_item_names
            input_available = [
                item_name in available_items
                for item_name in recipe.input_items
            ]

            if all(input_available):
                spawnable_item_names.add(recipe.output_item)

        if prev_spawnable_item_names == spawnable_item_names:
            break

    # Add some trash items
    trash_copies = [
        TRASH_ITEM_NAME
        for _ in item_specs + list(spawnable_item_names)
    ]

    # Pack the items in a list
    new_items = list()
    for i, item_name in enumerate(sorted(spawnable_item_names) + trash_copies):
        position = Position(
            x=start_position.x + i*spawn_gap,
            y=start_position.y,
        )
        item = ItemSpec(
            item_name=item_name,
            position=position,
        )
        new_items.append(item)
    return new_items


class AntSimulationInstance:
    """Encapsulates the simulator state of an ant simulation. The ant is
    controlled through a time-varying parametric periodic signal.
    """

    def __init__(
        self,
        robot_spawn_position: tuple[float, float],
        num_legs: int,
        button_positions: list[tuple[float, float]],
        wall_specs: list[WallSpec],
        door_specs: list[DoorSpec],
        crafting_table_specs: list[CraftingTableSpec],
        animation_fps: None | float,
        item_specs: list[ItemSpec],
        dining_table_specs: list[DiningTableSpec],
        trashcan_specs: list[TrashcanSpec],
        arena_width: float,
        arena_depth: float,
        sub_step_s: float,  # how much time to advance for step()
        plate_dispenser_specs: list[PlateDispenserSpec],
        frying_pan_specs: list[FryingPanSpec],
        frying_recipes: list[FryingRecipe],
        recipes: list[Recipe],
    ):
        error_message = "Crafting tables not implemented!"
        assert len(crafting_table_specs) == 0, error_message

        self.recipes = recipes
        self.door_specs = door_specs
        self.button_positions = button_positions
        self.recipes = recipes
        self.frying_recipes = frying_recipes

        # Create an arena
        arena = make_arena(
            width=arena_width,
            depth=arena_depth,
            base_length=BASE_LENGTH,
            map_boundary_width=MAP_BOUNDARY_WIDTH,
            map_boundary_height=MAP_BOUNDARY_HEIGHT,
        )
        assert arena.worldbody is not None

        # Place buttons in the arena
        self.buttons = [
            Cylinder(
                x=x,
                y=y,
                r=BUTTON_RADIUS,
                h=BUTTON_RADIUS/10,
            )
            for (x, y) in button_positions
        ]
        self.button_roots = [make_cylinder(button) for button in self.buttons]
        for button_root in self.button_roots:
            button_spawn = arena.worldbody.add('site')
            button_spawn.attach(button_root)

        # Place robot in the arena.
        self.robot = make_robot(
            num_legs=num_legs,
            body_radius=BODY_RADIUS,
            body_size=BODY_SIZE,
        )
        height = BODY_RADIUS*1.5
        spawn_pos = (robot_spawn_position[0], robot_spawn_position[1], height)
        spawn_site = arena.worldbody.add('site', pos=spawn_pos, group=3)
        spawn_site.attach(self.robot).add('freejoint')

        # Instantiate walls
        walls = [
            Box(
                x=wall_spec.position.x,
                y=wall_spec.position.y,
                w=wall_spec.depth,  # width and depth are inverted
                l=wall_spec.width,
                h=wall_spec.height,
            )
            for wall_spec in wall_specs
        ]
        for i, wall in enumerate(walls):
            wall_body = make_box(
                wall,
                get_wall_color(),
                identifier=f"wall_{i}",
            )
            spawn = arena.worldbody.add('site')
            spawn.attach(wall_body)

        # Instantiate doors
        doors = [
            Box(
                x=door_spec.position.x,
                y=door_spec.position.y,
                w=door_spec.depth,  # width and depth are inverted
                l=door_spec.width,
                h=door_spec.height,
            )
            for door_spec in door_specs
        ]
        self.door_bodies = list()
        for i, door in enumerate(doors):
            door_body = make_box(
                door,
                get_door_color(),
                identifier=f"door_{i}",
            )
            self.door_bodies.append(door_body)
            spawn = arena.worldbody.add('site')
            spawn.attach(door_body)

        # Instantiate crafting tables
        crafting_tables = [
            Box(
                x=crafting_table_spec.position.x,
                y=crafting_table_spec.position.y,
                w=CRAFTING_TABLE_SIZE[0],
                l=CRAFTING_TABLE_SIZE[1],
                h=CRAFTING_TABLE_SIZE[2],
            )
            for crafting_table_spec in crafting_table_specs
        ]
        for i, crafting_table in enumerate(crafting_tables):
            crafting_table_body = make_box(
                crafting_table,
                get_crafting_table_color(),
                identifier=f"crafting_table_{i}",
            )
            spawn = arena.worldbody.add('site')
            spawn.attach(crafting_table_body)

        # Instantiate trash cans
        trashcan_geom_names = list[str]()
        trashcans = [
            Box(
                x=trashcan_spec.position.x,
                y=trashcan_spec.position.y,
                w=trashcan_spec.width,
                l=trashcan_spec.depth,
                h=trashcan_spec.height,
            )
            for trashcan_spec in trashcan_specs
        ]
        for i, trashcan in enumerate(trashcans):
            identifier = f"trashcan_{i}"
            body = make_box(
                trashcan,
                get_trashcan_color(),
                identifier=identifier,
            )
            spawn = arena.worldbody.add('site')
            spawn.attach(body)
            trashcan_geom_names.append(identifier)

        # Instantiate dining tables
        dining_table_geom_names = list[str]()
        dining_tables = [
            Box(
                x=dining_table_spec.position.x,
                y=dining_table_spec.position.y,
                w=dining_table_spec.width,
                l=dining_table_spec.depth,
                h=dining_table_spec.height,
            )
            for dining_table_spec in dining_table_specs
        ]
        for i, dining_table in enumerate(dining_tables):
            identifier = f"dining_table_{i}"
            body = make_box(
                dining_table,
                get_dining_table_color(),
                identifier=identifier,
            )
            spawn = arena.worldbody.add('site')
            spawn.attach(body)
            dining_table_geom_names.append(identifier)

        # Instantiate frying pans
        frying_pan_ids = list[str]()
        frying_pans = [
            Box(
                x=frying_pan_spec.position.x,
                y=frying_pan_spec.position.y,
                w=frying_pan_spec.width,
                l=frying_pan_spec.depth,
                h=frying_pan_spec.height,
            )
            for frying_pan_spec in frying_pan_specs
        ]
        for i, frying_pan in enumerate(frying_pans):
            identifier = f"frying_pan_{i}"
            body = make_box(
                frying_pan,
                get_frying_pan_color(),
                identifier=identifier,
            )
            spawn = arena.worldbody.add('site')
            spawn.attach(body)
            frying_pan_ids.append((body, identifier))

        # Get spawnable items in the waiting area
        spawnable_item_specs = get_spawnable_item_specs(
            recipes=recipes,
            item_specs=item_specs,
            frying_recipes=frying_recipes,
        )

        # Add items
        item_models = list[tuple[str, Any, ItemSpec, Any]]()
        self.all_item_specs = item_specs + spawnable_item_specs
        for i, item in enumerate(self.all_item_specs):
            identifier = f"item_{i}_{item.item_name}"
            model = get_item(item.item_name, identifier=identifier)
            spawn_pos = (
                item.position.x,
                item.position.y,
                ITEM_DEFAULT_HEIGHT,
            )
            spawn = arena.worldbody.add(
                'site',
                pos=spawn_pos,
            )
            joint = spawn.attach(model).add('freejoint')
            item_models.append((identifier, model, item, joint))
        self.arena = arena

        # Initiate simulation
        physics = mjcf.Physics.from_mjcf_model(arena)
        assert physics is not None
        self.physics = physics

        # Bookkeep the names of the geoms that correspond to spawned items
        spawned_item_geom_names = list[str]()
        nonspawned_item_geom_names = list[str]()

        # Keep a map of geom IDs to item specs
        self.geom_names_to_models = dict()
        self.geom_names_to_specs = dict[str, ItemSpec]()
        self.geom_names_to_joints = dict()
        for (identifier, model, item, joint) in item_models:
            # Get the identifier from the root of the simulation
            geom = model.find('geom', identifier)

            geom_name = physics.model.id2name(
                physics.bind(geom).element_id,
                "geom",
            )
            self.geom_names_to_models[geom_name] = model
            self.geom_names_to_specs[geom_name] = item
            self.geom_names_to_joints[geom_name] = joint

            # Bookkeeping
            if item in item_specs:
                spawned_item_geom_names.append(geom_name)
            else:
                nonspawned_item_geom_names.append(geom_name)

        frying_pan_geom_names = list[str]()
        for model, identifier in frying_pan_ids:
            geom = model.find('geom', identifier)
            geom_name = physics.model.id2name(
                physics.bind(geom).element_id,
                "geom",
            )
            frying_pan_geom_names.append(geom_name)

        # Prepare animation data structure
        if animation_fps is not None:
            self.camera = engine.MovableCamera(
                self.physics,
                width=640,
                height=320,
            )
        else:
            self.camera = None
        self.animation_fps = animation_fps
        self.video = []

        # Keep track of the list of actuators
        self.torsos = self.robot.find('geom', 'torso')
        self.actuators = self.robot.find_all('actuator')
        self.joints = self.robot.find_all('joint')

        # Define sub-step number
        self.sub_step_s = sub_step_s

        # Keep track of all collisions during sub-steps
        self.step_item_item_contacts = list[tuple[str, str]]()

        # State tracking
        self._task_state = TaskState(
            activated_buttons=list(),
            crafted_items_log=list(),
            opened_doors=list(),
            spawned_item_geom_names=spawned_item_geom_names,
            nonspawned_item_geom_names=nonspawned_item_geom_names,
            trashcan_geom_names=trashcan_geom_names,
            dining_table_geom_names=dining_table_geom_names,
            frying_pan_geom_names=frying_pan_geom_names,
        )
        self.initial_state = self.state

        self.video_start_time = self.physics.data.time

        # Initial step to move items to their positions before in-class
        # collision tracking
        self.physics.step()

    @property
    def state(self) -> SimulationState:
        """Return the state of the simulation."""
        return SimulationState(
            mujoco_state=self.physics.get_state().tolist(),
            task_state=self._task_state,
        )

    @state.setter
    def state(self, state: SimulationState):
        self.physics.set_state(np.array(state.mujoco_state))
        self._task_state = state.task_state

    def reset(self):
        """Reset the simulation state to the beginning of the simulation."""
        self.state = self.initial_state

    def spawn_item(self, item: ItemSpec):
        """Teleport the "unspawned" geom into the simulation.

        This method assumes there is a matching "unspawned" item in the
        simulation (i.e., placed in the "waiting" area).
        """
        # Find unspawned geom that matches item
        to_be_spawned_geom_name: None | str = None
        for geom_name in self._task_state.nonspawned_item_geom_names:
            unspawned_item = self.geom_names_to_specs[geom_name]
            if item.item_name == unspawned_item.item_name:
                to_be_spawned_geom_name = geom_name

        error_message = f"No unspawned item '{item.item_name}'!"
        assert to_be_spawned_geom_name is not None, error_message

        # Move item model to the position of the item
        joint = self.geom_names_to_joints[to_be_spawned_geom_name]
        self.physics.bind(joint).qpos[0] = item.position.x
        self.physics.bind(joint).qpos[1] = item.position.y
        self.physics.bind(joint).qpos[2] = ITEM_DEFAULT_HEIGHT

        # Update geom name bookkeeping sets
        self._task_state.nonspawned_item_geom_names.remove(
            to_be_spawned_geom_name
        )
        self._task_state.spawned_item_geom_names.append(
            to_be_spawned_geom_name
        )

    def remove_item(
        self,
        geom_name: str,
        target_x: float = 100.0,
        target_y: float = 100.0,
    ):
        """Teleport the spawned geom into the "waiting area"."""
        # Move item model to the position of the item
        joint = self.geom_names_to_joints[geom_name]
        self.physics.bind(joint).qpos[0] = target_x
        self.physics.bind(joint).qpos[1] = target_y
        self.physics.bind(joint).qpos[2] = ITEM_DEFAULT_HEIGHT
        self.physics.bind(joint).qacc[0] = 0.0
        self.physics.bind(joint).qacc[1] = 0.0
        self.physics.bind(joint).qacc[2] = 0.0

        # Update geom name bookkeeping sets
        self._task_state.nonspawned_item_geom_names.append(geom_name)
        self._task_state.spawned_item_geom_names.remove(geom_name)

    @property
    def current_image(self) -> NDArray[np.uint8 | np.int32]:
        assert self.camera is not None
        t = self.physics.data.time
        camera_rotation_degrees = CAMERA_ROTATION_SPEED_DEG_S*t
        self.camera.set_pose(
            lookat=self.torso_position,
            distance=CAMERA_DISTANCE,
            azimuth=CAMERA_AZIMUTH_DEGREES+camera_rotation_degrees,
            elevation=CAMERA_ELEVATION_DEGREES,
        )
        pixels = self.camera.render()
        return pixels

    def sub_step(self, action: np.ndarray):
        """Step the simulation once.

        The given `actions` is an array of `len(self.actuators)` numbers
        between that 0 and 1 that define the phase of each actuator.

        There are two actuators per leg.
        """
        # Inject controls and step the physics.
        action = np.clip(action, -1.0, 1.0)
        self.physics.bind(self.actuators).ctrl = action
        self.physics.step()

        # Log item-item contacts in every sub-step
        self.step_item_item_contacts.extend(self.item_item_contacts)

        # Domain semantics (logic not in the physics engine itself)
        self.process_button_presses()
        self.process_item_item_contacts()
        self.process_trashcans()
        self.process_dining_tables()
        self.process_frying_pans()

        # Record animation frame if appropriate
        if self.animation_fps is not None and self.camera is not None:
            video_time= self.physics.data.time - self.video_start_time
            target_video_n = video_time*self.animation_fps
            if len(self.video) < target_video_n:
                pixels = self.current_image
                self.video.append(pixels.copy())

    def process_trashcans(self):
        """Handles item-trashcan contacts, 'destroying' items that
        touch a trashcan."""
        for i in range(self.physics.data.ncon):
            contact = self.physics.data.contact[i]
            geom1_id = contact.geom1
            geom2_id = contact.geom2

            is_trashcan_involved = any((
                geom1_id in self._task_state.trashcan_geom_names,
                geom2_id in self._task_state.trashcan_geom_names,
            ))

            is_spawned_item_involved = any((
                geom1_id in self._task_state.spawned_item_geom_names,
                geom2_id in self._task_state.spawned_item_geom_names,
            ))

            # If no trashcan is involved, ignore collision
            if not is_trashcan_involved:
                continue

            # If no spawned item is involved, ignore collision
            if not is_spawned_item_involved:
                continue

            # Get model of spawned item that collided with trashcan
            item_geom_id: None | str = None
            for geom_id in [geom1_id, geom2_id]:
                if geom_id in self._task_state.spawned_item_geom_names:
                    item_geom_id = geom_id
                    break
            assert item_geom_id is not None
            self.remove_item(item_geom_id)

    def process_dining_tables(self):
        """Handles item-dining table contacts.

        When an item touches a dining table, it is 'destroyed' and a 'trash'
        spawn at the dining table is scheduled.
        """
        for i in range(self.physics.data.ncon):
            contact = self.physics.data.contact[i]
            geom1_id = contact.geom1
            geom2_id = contact.geom2

            is_dining_table_involved = any((
                geom1_id in self._task_state.dining_table_geom_names,
                geom2_id in self._task_state.dining_table_geom_names,
            ))

            is_spawned_item_involved = any((
                geom1_id in self._task_state.spawned_item_geom_names,
                geom2_id in self._task_state.spawned_item_geom_names,
            ))

            # If no dining_table is involved, ignore collision
            if not is_dining_table_involved:
                continue

            # If no spawned item is involved, ignore collision
            if not is_spawned_item_involved:
                continue

            # Get model of spawned item that collided with dining_table
            item_geom_id: None | str = None
            for geom_id in [geom1_id, geom2_id]:
                if geom_id in self._task_state.spawned_item_geom_names:
                    item_geom_id = geom_id
                    break
            assert item_geom_id is not None
            self.remove_item(item_geom_id)

            # Spawn a trash item
            spawn_position = Position(
                contact.pos[0],
                contact.pos[1],
            )
            item_spec = ItemSpec(
                item_name=TRASH_ITEM_NAME,
                position=spawn_position,
            )
            self.spawn_item(item_spec)

    def process_item_item_contacts(self):
        """Handles item-item contacts, determining
        if a crafting recipe was activated. The method
        'spawns' and 'destroys' the corresponding items and updates
        the crafting log."""
        # Check for collisions between objects
        new_item_specs = list[ItemSpec]()
        to_be_removed_geom_names = set[str]()
        for i in range(self.physics.data.ncon):
            contact = self.physics.data.contact[i]
            geom1_id = contact.geom1
            geom2_id = contact.geom2

            # Retrieve names of the geoms involved in the collision
            geom1_name = self.physics.model.id2name(geom1_id, 'geom')
            geom2_name = self.physics.model.id2name(geom2_id, 'geom')

            # Check if any of the geoms are spawned items
            collision_items = list[str]()
            for geom_name in [geom1_name, geom2_name]:
                if geom_name in self._task_state.spawned_item_geom_names:
                    spec = self.geom_names_to_specs[geom_name]
                    collision_items.append(spec.item_name)

            if len(collision_items) != 2:
                continue

            # If the collision matches a recipe, add the item
            for recipe in self.recipes:
                if sorted(recipe.input_items) == sorted(collision_items):
                    spawn_position = Position(
                        contact.pos[0],
                        contact.pos[1],
                    )
                    new_item_spec = ItemSpec(
                        item_name=recipe.output_item,
                        position=spawn_position,
                    )
                    new_item_specs.append(new_item_spec)
                    to_be_removed_geom_names.add(geom1_name)
                    to_be_removed_geom_names.add(geom2_name)

        for i, geom_name in enumerate(to_be_removed_geom_names):
            self.remove_item(geom_name)
        for item in new_item_specs:
            self._task_state.crafted_items_log.append(item.item_name)
            self.spawn_item(item)

    def process_frying_pans(self):
        """Handles item-frying pan contacts, determining
        if a frying recipe was activated. This method
        'spawns' and 'destroys' the corresponding items and updates
        the crafting log."""
        # Check for collisions between objects
        new_item_specs = list[ItemSpec]()
        to_be_removed_geom_names = set[str]()
        for i in range(self.physics.data.ncon):
            contact = self.physics.data.contact[i]
            geom1_id = contact.geom1
            geom2_id = contact.geom2

            # Retrieve names of the geoms involved in the collision
            geom1_name = self.physics.model.id2name(geom1_id, 'geom')
            geom2_name = self.physics.model.id2name(geom2_id, 'geom')

            # Check if any of the geoms are frying pans
            frying_pan_involved = any((
                geom1_name in self._task_state.frying_pan_geom_names,
                geom2_name in self._task_state.frying_pan_geom_names,
            ))

            if not frying_pan_involved:
                continue

            # Check if any of the geoms are spawned items
            collision_items = list[str]()
            for geom_name in [geom1_name, geom2_name]:
                if geom_name in self._task_state.spawned_item_geom_names:
                    spec = self.geom_names_to_specs[geom_name]
                    collision_items.append(spec.item_name)

            if not len(collision_items) == 1:
                continue
            collision_item = collision_items[0]

            # If the collision matches a frying recipe, spawn the output item
            # and remove the input item
            for recipe in self.frying_recipes:
                if recipe.input_item == collision_item:
                    spawn_position = Position(
                        contact.pos[0],
                        contact.pos[1],
                    )
                    new_item_spec = ItemSpec(
                        item_name=recipe.output_item,
                        position=spawn_position,
                    )
                    new_item_specs.append(new_item_spec)

                    for geom_name in [geom1_name, geom2_name]:
                        if geom_name in self._task_state.spawned_item_geom_names:
                            to_be_removed_geom_names.add(geom_name)
                            break

        for i, geom_name in enumerate(to_be_removed_geom_names):
            self.remove_item(geom_name)
        for item in new_item_specs:
            self._task_state.crafted_items_log.append(item.item_name)
            self.spawn_item(item)

    def process_button_presses(self):
        """Determines if a button was pressed by checking the contacts
        at the current simulation step. If a button was pressed,
        it is activated."""
        position = np.array(self.torso_horizontal_position)
        for i, (button_x, button_y) in enumerate(self.button_positions):
            button_position = np.array((button_x, button_y))
            displacement = position - button_position
            distance = np.linalg.norm(displacement)
            if distance < BUTTON_RADIUS:
                self.activate_button(i)

    @property
    def item_contacts_with_robot(self) -> list[str]:
        """Return the list of item names that are in contact with the robot."""
        items_names = list()

        # Identify robot geoms
        # First, get a reference from the root of the simulation to
        # each geom. Then, get the IDs of the geoms and convert them to
        # full names (e.g., 'unamed_model_2/torso')
        robot_geoms = self.robot.find_all("geom")
        robot_geom_names = [
            self.physics.model.id2name(
                self.physics.bind(geom).element_id,
                "geom"
            )
            for geom in robot_geoms
        ]

        for i in range(self.physics.data.ncon):
            contact = self.physics.data.contact[i]
            geom1_id = contact.geom1
            geom2_id = contact.geom2

            # Retrieve names of the geoms involved in the collision
            geom1_name = self.physics.model.id2name(geom1_id, 'geom')
            geom2_name = self.physics.model.id2name(geom2_id, 'geom')

            # Check if any of the two geoms belongs to the robot
            one_is_robot = any((
                geom1_name in robot_geom_names,
                geom2_name in robot_geom_names,
            ))
            if not one_is_robot:
                continue

            # Check if any of the two geoms belongs to an item
            one_is_item = any((
                geom_name in self.geom_names_to_specs.keys()
                for geom_name in [geom1_name, geom2_name]
            ))
            if not one_is_item:
                continue

            # Identify the item
            if geom1_name in self.geom_names_to_specs.keys():
                item_name = self.geom_names_to_specs[geom1_name].item_name
            else:
                item_name = self.geom_names_to_specs[geom2_name].item_name
            items_names.append(item_name)

        return items_names

    @property
    def item_item_contacts(self) -> list[tuple[str, str]]:
        """Return the pairs of items that are in contact."""
        # Check for collisions between objects
        item_item_contacts = list[tuple[str, str]]()
        for i in range(self.physics.data.ncon):
            contact = self.physics.data.contact[i]
            geom1_id = contact.geom1
            geom2_id = contact.geom2

            # Retrieve names of the geoms involved in the collision
            geom1_name = self.physics.model.id2name(geom1_id, 'geom')
            geom2_name = self.physics.model.id2name(geom2_id, 'geom')

            # Check if any of the geoms are spawned items
            collision_items = list[str]()
            for geom_name in [geom1_name, geom2_name]:
                if geom_name in self._task_state.spawned_item_geom_names:
                    spec = self.geom_names_to_specs[geom_name]
                    collision_items.append(spec.item_name)

            if len(collision_items) != 2:
                continue

            item1, item2 = collision_items
            item_item_contacts.append((item1, item2))

        return item_item_contacts

    def step(self, action: np.ndarray) -> None:
        """Step the simulation until `self.sub_step_s` seconds have elapsed.

        The given `actions` is an array of `len(self.actuators)` numbers
        between that 0 and 1 that define the target position of each actuator.
        """
        start_t = self.physics.data.time
        self.step_item_item_contacts = list[tuple[str, str]]()
        while self.physics.data.time - start_t < self.sub_step_s:
            self.sub_step(action)

    @property
    def simulation_time_s(self) -> float:
        """Return the current simulation time."""
        return self.physics.data.time

    @property
    def torso_position(self) -> tuple[float, float, float]:
        """Return the horizontal position of the torso of the ant."""
        return (
            self.physics.bind(self.torsos).xpos[0].copy(),
            self.physics.bind(self.torsos).xpos[1].copy(),
            self.physics.bind(self.torsos).xpos[2].copy()
        )

    @property
    def torso_horizontal_position(self) -> tuple[float, float]:
        """Return the horizontal position of the torso of the ant."""
        return (
            self.physics.bind(self.torsos).xpos[0].copy(),
            self.physics.bind(self.torsos).xpos[1].copy()
        )

    @property
    def torso_euler_angles(self) -> tuple[float, float, float]:
        """Return the Euler rotation angles of the torso."""
        xmat = self.physics.bind(self.torsos).xmat.copy().tolist()
        # xmat is stored in row-major format
        rotation_matrix = [
            [xmat[0], xmat[1], xmat[2]],
            [xmat[3], xmat[4], xmat[5]],
            [xmat[6], xmat[7], xmat[8]],
        ]
        euler_angles = get_euler_angles(rotation_matrix)
        return euler_angles

    @property
    def actuator_state(self) -> tuple[float, ...]:
        """Return the state of the actuators."""
        actuator_state = [
            *list(self.physics.data.qpos),
            *list(self.physics.data.qvel),
        ]
        return tuple(actuator_state)

    def remove_doors(self):
        """Remove all the doors in the map."""
        for body in self.door_bodies:
            for geom in body.find_all('geom'):
                o = self.physics.bind(geom)
                o.pos = (o.pos[0], o.pos[1], -5)

    def activate_button(self, i: int):
        """Push the button."""

        # Change button color
        self.state.task_state.activated_buttons.append(i)
        for geom in self.button_roots[i].find_all('geom'):
            self.physics.bind(geom).rgba = ([0.5, 0.2, 0.2, 1])

        # Remove doors
        self.remove_doors()

    def clear_video(self):
        """Clear the video so far."""
        self.video.clear()
        self.video_start_time = self.physics.data.time

    def save_video(self, path: Path):
        """Save the video recorded since the simulation was instantiated."""
        assert self.animation_fps is not None
        save_video(self.video, path, self.animation_fps)

    def get_item_positions(self, item_name: str) -> list[Position]:
        """Get the positions of all items that have the given item name."""
        # Get all Mujoco models for the given item name
        # TODO: only return information about spawned items
        models = list()
        for geom_name, item_model in self.geom_names_to_models.items():
            model_spec = self.geom_names_to_specs[geom_name]
            if item_name != model_spec.item_name:
                continue
            geom = item_model.find_all('geom')[0]
            models.append(geom)

        # Get position of the models
        raw_positions = self.physics.bind(models).xpos.copy().transpose(
            0, 1
        ).tolist()
        positions = [
            Position(x, y)
            for (x, y, _) in raw_positions
        ]
        return positions


def get_simulation_instance(
    map_spec: MapSpec,
    animation_fps: None | float,
    sub_step_s: float,  # how much time to advance for step() in seconds
) -> AntSimulationInstance:
    button_positions = [
        (button.position.x, button.position.y)
        for button in map_spec.buttons
    ]
    robot_spawn_position = (
        map_spec.robot.position.x,
        map_spec.robot.position.y,
    )
    simulation = AntSimulationInstance(
        robot_spawn_position=robot_spawn_position,
        num_legs=map_spec.robot.num_legs,
        wall_specs=map_spec.walls,
        button_positions=button_positions,
        animation_fps=animation_fps,
        sub_step_s=sub_step_s,
        door_specs=map_spec.doors,
        crafting_table_specs=map_spec.crafting_tables,
        dining_table_specs=map_spec.dining_tables,
        trashcan_specs=map_spec.trashcans,
        item_specs=map_spec.items,
        arena_width=map_spec.width,
        arena_depth=map_spec.depth,
        plate_dispenser_specs=map_spec.plate_dispensers,
        frying_pan_specs=map_spec.frying_pans,
        frying_recipes=map_spec.frying_recipes,
        recipes=map_spec.recipes,
    )
    return simulation


def get_items(simulation: AntSimulationInstance) -> list[ItemSpec]:
    """Get the state of spawned items for the given simulation."""
    items = list[ItemSpec]()
    spawned_geom_names = [
        geom_name
        for geom_name in simulation.state.task_state.spawned_item_geom_names
    ]
    items = [
        simulation.geom_names_to_specs[geom_name]
        for geom_name in spawned_geom_names
    ]
    return items


def get_abstract_map_state(
    simulation: AntSimulationInstance,
) -> AbstractMapState:
    """Get the abstract state corresponding to the current state of the
    simulation."""
    state = simulation.state
    euler_angles = simulation.torso_euler_angles
    abstract_map_state = AbstractMapState(
        robot_position=Position(*simulation.torso_horizontal_position),
        robot_yaw_radians=euler_angles[0],
        robot_pitch_radians=euler_angles[1],
        robot_roll_radians=euler_angles[2],
        items=get_items(simulation),
        doors=simulation.door_specs,
        button_positions=simulation.button_positions,
        crafted_items_log=state.task_state.crafted_items_log,
        opened_doors=state.task_state.opened_doors,
        activated_buttons=state.task_state.activated_buttons,
        robot_item_contact_names=simulation.item_contacts_with_robot,
        item_item_contact_pairs=simulation.step_item_item_contacts,
    )
    return abstract_map_state
