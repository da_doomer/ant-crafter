from dm_control import mjcf
from dataclasses import dataclass
from ant_crafter.mujoco.xkcd_colors import get_random_color
from typing import TypeVar
from pathlib import Path
import random
import inspect
import sys


current_module = sys.modules[__name__]
object_dir = Path(
    inspect.getfile(current_module)
).parent.parent.parent/"lib"/"object_sim"


@dataclass(frozen=True)
class Cylinder:
    x: float
    y: float
    r: float
    h: float


@dataclass(frozen=True)
class Box:
    x: float
    y: float
    w: float
    l: float
    h: float


def make_cylinder(cylinder: Cylinder) -> mjcf.RootElement:
    """Make a cylindrical objetct."""
    root = mjcf.RootElement()
    # Place button
    root.worldbody.add(
        'geom',
        type='cylinder',
        size=(cylinder.r, cylinder.h),
        rgba=[0.2, 0.5, 0.2, 1],
        pos=(cylinder.x, cylinder.y, 0),
    )
    return root


def make_box(
    box: Box,
    color: tuple[float, float, float, float],
    identifier: str,
) -> mjcf.RootElement:
    """Make a cylindrical objetct."""
    root = mjcf.RootElement()

    # Place button
    root.worldbody.add(
        'geom',
        name=f'{identifier}',
        type='box',
        size=(box.w, box.l, box.h),
        rgba=color,
        pos=(box.x, box.y, box.h/2),
    )
    return root


T = TypeVar("T")


_random = random.Random("42")
def choose_randomly(
    items: list[T],
) -> T:
    item = _random.choice(items)
    return item


def get_wall_color() -> tuple[float, float, float, float]:
    # Choose color
    rgbas = [
        (0.2, 0.2, 0.2, 1),
        (0.25, 0.25, 0.25, 1),
    ]
    return choose_randomly(rgbas)


def get_door_color() -> tuple[float, float, float, float]:
    # Choose color
    rgbas = [
        (157/256, 125/256, 123/256, 1),
    ]
    return choose_randomly(rgbas)


def get_dining_table_color() -> tuple[float, float, float, float]:
    # Choose color
    rgbas = [
        (54/256, 73/256, 50/256, 1),
    ]
    return choose_randomly(rgbas)


def get_trashcan_color() -> tuple[float, float, float, float]:
    # Choose color
    rgbas = [
        (144/256, 14/256, 45/256, 1),
    ]
    return choose_randomly(rgbas)


def get_crafting_table_color() -> tuple[float, float, float, float]:
    # Choose color
    rgbas = [
        (157/256, 125/256, 123/256, 1),
    ]
    return choose_randomly(rgbas)


def get_frying_pan_color() -> tuple[float, float, float, float]:
    # Choose color
    rgbas = [
        (65/256, 14/256, 45/256, 1),
    ]
    return choose_randomly(rgbas)


def get_item(item_name: str, identifier: str) -> mjcf.RootElement:
    # Create a new MJCF model
    root = mjcf.RootElement()

    # Add a mesh asset
    file = (object_dir/item_name/f"{item_name}.stl").resolve()
    assert file.exists(), f"Object file '{file.resolve()}' not found!"
    mesh = root.asset.add(
        'mesh',
        name=f'{identifier}',
        file=str(file),
        scale=[2.0, 2.0, 2.0],
    )

    # Add a geom to the model that uses the mesh
    color = get_random_color(seed=item_name)
    geom = root.worldbody.add(
        'geom',
        type='mesh',
        mesh=mesh,
        name=f'{identifier}',
        rgba=color,
        density=50,  # default is 1000, water density
    )

    return root
