"""Sample colors from the XKCD color list."""
from pathlib import Path
from functools import cache
import random
import csv


color_file = (Path(__file__).parent/"rgb.txt").resolve()
assert color_file.exists(), f"Color file '{color_file}' not found!"


@cache
def get_xkcd_colors() -> dict[str, tuple[float, float, float, float]]:
    """Returns a mapping from human-readable name string to [0, 1]-valued RGBA
    colors from the list of XKCD colors.

    All colors have alpha of 1.0.
    """
    colors = dict()
    with open(color_file, "rt") as fp:
        next(fp)
        color_reader = csv.reader(fp, delimiter='\t')
        for row in color_reader:
            color_name, hexcolor, _ = row
            rgb = tuple(
                int(hexcolor.lstrip('#')[i:i+2], 16)/256
                for i in (0, 2, 4)
            )
            colors[color_name] = (*rgb, 1.0)
    return colors


def get_random_color(seed: str) -> tuple[float, float, float, float]:
    """Returns [0, 1]-valued RGBA encoding of a random color.
    The color is chosen from the list of XKCD colors.

    All colors have alpha of 1.0.
    """
    _random = random.Random(seed)
    colors = sorted(list(get_xkcd_colors().values()))
    color = _random.choice(colors)
    return color
