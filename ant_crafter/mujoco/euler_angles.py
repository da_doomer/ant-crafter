"""Euler angles utilities."""


from math import asin, atan2, cos
import math


RotationMatrix = tuple[
    tuple[float, float, float],
    tuple[float, float, float],
    tuple[float, float, float],
]

EulerAngles = tuple[float, float, float]


def get_euler_angles(rotation_matrix: RotationMatrix) -> EulerAngles:
    """Get the Euler angles in radians that correspond to the given rotation
    matrix."""
    # Credit:
    # "Computing Euler angles from a rotation matrix"
    # Gregory G. Slabaugh
    R = rotation_matrix

    if abs(R[2][0]) != 1:
        theta1 = -asin(R[2][0])
        theta2 = math.pi - theta1
        psi1 = atan2(R[2][1]/cos(theta1), R[2][2]/cos(theta1))
        psi2 = atan2(R[2][1]/cos(theta2), R[2][2]/cos(theta2))
        phi1 = atan2(R[1][0]/cos(theta1), R[0][0]/cos(theta1))
        phi2 = atan2(R[1][0]/cos(theta2), R[0][0]/cos(theta2))

        solution1 = (theta1, psi1, phi1)
        solution2 = (theta2, psi2, phi2)

        # Both solutions are valid, we choose the one with the smallest angles
        sum1 = sum(map(abs, solution1))
        sum2 = sum(map(abs, solution2))

        if sum1 < sum2:
            return solution1
        return solution2

    phi = 0  # can be anything
    if R[2][0] == -1:
        theta = math.pi/2
        psi = phi + atan2(R[0][1], R[0][2])
    else:
        theta = -math.pi/2
        psi = -phi + atan2(-R[0][1], -R[0][2])

    return (theta, psi, phi)
