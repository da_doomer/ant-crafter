from dm_control import mjcf
import numpy as np


random_state = np.random.RandomState(42)


class Leg(object):
    """A 2-DoF leg with position actuators.

    Source:
    https://colab.research.google.com/github/deepmind/dm_control/blob/main/tutorial.ipynb#scrollTo=gKny5EJ4uVzu
    """

    def __init__(self, length, rgba):
        self.model = mjcf.RootElement()

        # Defaults:
        self.model.default.joint.damping = 2
        self.model.default.joint.type = 'hinge'
        self.model.default.geom.type = 'capsule'
        self.model.default.geom.rgba = rgba  # Continued below...

        # Thigh:
        self.thigh = self.model.worldbody.add('body')
        self.hip = self.thigh.add('joint', axis=[0, 0, 1])
        self.thigh.add(
            'geom',
            fromto=[0, 0, 0, length, 0, 0],
            size=[length/4],
            name="thigh",
        )

        # Hip:
        self.shin = self.thigh.add('body', pos=[length, 0, 0])
        self.knee = self.shin.add('joint', axis=[0, 1, 0])
        self.shin.add(
            'geom',
            fromto=[0, 0, 0, 0, 0, -length],
            size=[length/5],
            name="shin",
        )

        # Position actuators:
        self.model.actuator.add('position', joint=self.hip, kp=10)
        self.model.actuator.add('position', joint=self.knee, kp=10)


def make_robot(
    num_legs: int,
    body_size: tuple[float, float, float],
    body_radius: float,
) -> mjcf.RootElement:
    """Constructs a creature with `num_legs` legs.

    Source:
    https://colab.research.google.com/github/deepmind/dm_control/blob/main/tutorial.ipynb#scrollTo=SESlL_TidKHx
    """
    model = mjcf.RootElement()
    model.compiler.angle = 'radian'  # Use radians.
    rgba = random_state.uniform([0, 0, 0, 1], [1, 1, 1, 1])

    # Make the torso geom.
    model.worldbody.add(
        'geom',
        name='torso',
        type='ellipsoid',
        size=body_size,
        rgba=rgba
    )

    # Attach legs to equidistant sites on the circumference.
    for i in range(num_legs):
        theta = 2 * i * np.pi / num_legs
        hip_pos = body_radius * np.array([np.cos(theta), np.sin(theta), 0])
        hip_site = model.worldbody.add(
            'site',
            pos=hip_pos,
            euler=[0, 0, theta]
        )
        leg = Leg(length=body_radius, rgba=rgba)
        hip_site.attach(leg.model)
    return model
