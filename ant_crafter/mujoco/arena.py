from dm_control import mjcf
from ant_crafter.mujoco.objects import Box


def get_boundary_color() -> tuple[float, float, float, float]:
    return (0.85, 0.85, 0.85, 0.2)


def make_arena(
    width: float,
    depth: float,
    base_length: float,
    map_boundary_width: float,
    map_boundary_height: float,
) -> mjcf.RootElement:
    """ Create a flat arena.

    Source:
    https://colab.research.google.com/github/deepmind/dm_control/blob/main/tutorial.ipynb#scrollTo=F7_Tx9P9U_VJ
    """
    arena = mjcf.RootElement()

    # Add map floor
    chequered = arena.asset.add(
        'texture',
        type='2d',
        builtin='checker',
        width=300,
        height=300,
        rgb1=[.3, .4, .5],
        rgb2=[.3, .4, .5]
    )
    grid = arena.asset.add(
        'material',
        name='grid',
        texture=chequered,
        texrepeat=[width//base_length//2, width//base_length//2],
        reflectance=.2
    )
    arena.worldbody.add(
        'geom',
        type='plane',
        size=[width/2, depth/2, .1],  # half-size according to the docs
        pos=[width/2, depth/2, 0.0],
        material=grid
    )
    # TODO: dynamic lightning so that large maps are lit
    for x in [-2, 2]:
        arena.worldbody.add('light', pos=[x, -1, 3], dir=[-x, 1, -2])

    # Add infinite floor below
    flat = arena.asset.add(
        'texture',
        type='2d',
        builtin='flat',
        width=300,
        height=300,
        rgb1=[.9, .9, .9],
        rgb2=[.9, .9, .9]
    )
    infinite = arena.asset.add(
        'material',
        name='infinite_floor',
        texture=flat,
        rgba=[0, 0, 0, 0],
    )
    arena.worldbody.add(
        'geom',
        type='plane',
        size=[0, 0, 1],  # zero first two coordinates for infinite plane
        pos=[0, 0, -base_length*10],
        material=infinite
    )

    # Add "invisible" walls to act as map boundary
    walls = list()

    # Instantiate walls
    walls = [
        Box(
            x=width/2,
            y=-map_boundary_width/2,
            w=width/2,
            l=map_boundary_width,
            h=map_boundary_height,
        ),
        Box(
            x=width/2,
            y=depth+map_boundary_width/2,
            w=width/2,
            l=map_boundary_width,
            h=map_boundary_height,
        ),
        Box(
            x=-map_boundary_width,
            y=depth/2,
            w=map_boundary_width,
            l=depth/2,
            h=map_boundary_height,
        ),
        Box(
            x=width+map_boundary_width/2,
            y=depth/2,
            w=map_boundary_width,
            l=depth/2,
            h=map_boundary_height,
        ),
    ]
    for box in walls:
        arena.worldbody.add(
            'geom',
            type='box',
            size=(box.w, box.l, box.h),
            rgba=get_boundary_color(),
            pos=(box.x, box.y, box.h),
        )
    return arena
