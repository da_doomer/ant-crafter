"""A map description language for the Crafting Ant environment."""
from dataclasses import dataclass
from collections.abc import Iterator


@dataclass
class Position:
    x: float
    y: float

    def __iter__(self) -> Iterator[float]:
        return iter((self.x, self.y))


@dataclass
class RobotSpec:
    position: Position
    num_legs: int

    @property
    def actuator_n(self) -> int:
        return self.num_legs*2


@dataclass
class ItemSpec:
    item_name: str
    position: Position


@dataclass
class CraftingTableSpec:
    position: Position


@dataclass
class KeySpec:
    position: Position


@dataclass
class WallSpec:
    position: Position
    width: float
    height: float
    depth: float


@dataclass
class DoorSpec:
    position: Position
    width: float
    height: float
    depth: float


@dataclass
class ButtonSpec:
    position: Position


@dataclass
class Recipe:
    input_items: list[str]
    output_item: str


@dataclass
class FryingRecipe:
    input_item: str
    output_item: str


@dataclass
class DiningTableSpec:
    position: Position
    width: float
    height: float
    depth: float


@dataclass
class TrashcanSpec:
    position: Position
    width: float
    height: float
    depth: float


@dataclass
class FryingPanSpec:
    position: Position
    width: float
    height: float
    depth: float


@dataclass
class PlateDispenserSpec:
    position: Position
    width: float
    height: float
    depth: float


@dataclass
class MapSpec:
    robot: RobotSpec
    items: list[ItemSpec]
    crafting_tables: list[CraftingTableSpec]
    walls: list[WallSpec]
    dining_tables: list[DiningTableSpec]
    trashcans: list[TrashcanSpec]
    doors: list[DoorSpec]
    buttons: list[ButtonSpec]
    width: float
    depth: float
    frying_pans: list[FryingPanSpec]
    plate_dispensers: list[PlateDispenserSpec]
    frying_recipes: list[FryingRecipe]
    recipes: list[Recipe]


@dataclass
class AbstractMapState:
    robot_position: Position
    robot_yaw_radians: float
    robot_pitch_radians: float
    robot_roll_radians: float
    items: list[ItemSpec]
    doors: list[DoorSpec]
    button_positions: list[tuple[float, float]]
    crafted_items_log: list[str]
    opened_doors: list[int]
    activated_buttons: list[int]
    robot_item_contact_names: list[str]
    item_item_contact_pairs: list[tuple[str, str]]
