"""Script to sample sequences of items."""
from ant_crafter.mujoco.map import Recipe
import random


RecipeBook = list[Recipe]
ItemName = str


def get_all_items(
    recipe_book: RecipeBook,
) -> list[ItemName]:
    """Return the names of all items in the recipe book."""
    all_items = list()
    for recipe in recipe_book:
        all_items.extend(recipe.input_items)
        all_items.append(recipe.output_item)
    return all_items


def get_leaf_items(
    recipe_book: RecipeBook,
) -> list[ItemName]:
    """Return the names of items that cannot be crafted with
    other items."""
    all_items = get_all_items(recipe_book)
    craftable = [
        recipe.output_item
        for recipe in recipe_book
    ]
    not_craftable = [
        item
        for item in all_items
        if item not in craftable
    ]
    return not_craftable


def get_boundary(
    recipe_book: RecipeBook,
    crafted_items: list[ItemName],
) -> list[ItemName]:
    """The boundary consists of all those items that are not crafted and are
    made out of crafted items."""
    boundary = list()
    for recipe in recipe_book:
        if recipe.output_item in crafted_items:
            continue
        prerequisites_crafted = [
            prereq in crafted_items
            for prereq in recipe.input_items
        ]
        if not all(prerequisites_crafted):
            continue
        boundary.append(recipe.output_item)
    return boundary


def get_item_sequence(
    recipe_book: RecipeBook,
    size: int,
    seed: str,
) -> list[ItemName]:
    """Generates a sequence of items to craft."""
    sequence = list()

    _random = random.Random(seed)

    # All leaf items are initially marked as crafted
    crafted_items = get_leaf_items(recipe_book)

    # We will sample `size` times from the "boundary" of craftable items
    for _ in range(size):
        boundary = get_boundary(
            crafted_items=crafted_items,
            recipe_book=recipe_book,
        )
        item = _random.choice(boundary)
        crafted_items.append(item)
        sequence.append(item)

    return sequence
