"""Instantiate a map from a map template and sequence of items."""
from ant_crafter.mujoco.map import Recipe
from ant_crafter.mujoco.map import MapSpec
from ant_crafter.map_parser import get_map_spec, jsonify_recipe
from pathlib import Path
import tempfile
import random
import json


RecipeBook = list[Recipe]
ItemName = str


def is_input(
    candidate_item_name: ItemName,
    recipe_item_name: ItemName,
    recipe_book: RecipeBook,
) -> bool:
    """Return if the candidate item is an input to the recipe item
    according to the recipe book."""
    for recipe in recipe_book:
        if recipe.output_item != recipe_item_name:
            continue
        return candidate_item_name in recipe.input_items
    raise ValueError("Recipe item not in recipe book!")


def _get_initial_items(
    target_item: ItemName,
    recipe_book: RecipeBook,
) -> list[ItemName]:
    """Return the name of primitive items that have to be present in the map
    from the start in order to make `target_item`.

    A primitive item is a leaf item in the crafting tree induced by the recipe
    book.

    Assumptions:
    - Any given item is produced by at most one recipe.
    """
    # Search for a recipe that produces the target item
    producing_recipe: None | Recipe = None
    for recipe in recipe_book:
        if recipe.output_item == target_item:
            producing_recipe = recipe
            break

    is_primitive_item = producing_recipe is None

    # Case 1: target item is a primitive item
    if is_primitive_item:
        return [target_item]

    # Otherwise, it has dependencies
    needed_items = producing_recipe.input_items

    return needed_items


def get_initial_items(
    recipe_book: RecipeBook,
    item_sequence: list[ItemName],
) -> list[ItemName]:
    """Return the name of primitive items that have to be present in the map
    from the start in order to make all the items in the sequence craftable.

    A primitive item is a leaf item in the crafting tree induced by the recipe
    book.

    Assumptions:
    - Any given item is produced by at most one recipe.
    """
    needed_items = list()
    for item_name in item_sequence:
        item_needed_items = _get_initial_items(
            target_item=item_name,
            recipe_book=recipe_book,
        )
        for needed_item in item_needed_items:
            needed_items.append(needed_item)
    return list(needed_items)


def get_available_item_coordinates(
    map_template: list[str],
) -> list[tuple[int, int]]:
    """Get the coordinates where items can be placed in the given map
    template."""
    # The coordinate system is reversed in map files for human readability.
    #
    # Map:
    # ----
    # ---B
    # A---
    #
    # The first line in the map is `----`.
    # The character A is in coordinate (0,0)
    # The character B is in (3, 1)
    # So we reverse the map template to make the y-axis match the
    # iteration order
    map_template = list(reversed(map_template))
    item_coordinates = set()
    for y, line in enumerate(map_template):
        for x, char in enumerate(line):
            if char != "I":
                continue
            item_coordinates.add((x, y))
    return list(item_coordinates)


def get_map_instance(
    recipe_book: RecipeBook,
    map_template: list[str],
    item_sequence: list[ItemName],
    seed: str,
) -> MapSpec:
    """Get a randomly sampled map instance.

    **Assumption:** an item is present in at most one recipe.
    """
    _random = random.Random(seed)
    # The approach is to:
    # 1. Identify the leaf items that must be present in the map from the start.
    # 2. Sample the initial position of each leaf item.
    # 3. Generate the map JSON object

    # Identify leaf items that must be present from the start
    # **Assumption:** an item is present in at most one recipe. This
    # simplifies things.
    initial_items = get_initial_items(
        recipe_book=recipe_book,
        item_sequence=item_sequence,
    )

    # Identify available spawn positions. We have to get the coordinates right.
    available_coordinates = get_available_item_coordinates(map_template)

    # Sample initial position of each item.
    concrete_items = list()
    _random.shuffle(available_coordinates)
    for initial_item in initial_items:
        coordinate = available_coordinates.pop()
        item = {
            "item_name": initial_item,
            "position": coordinate,
        }
        concrete_items.append(item)

    # Generate the map JSON object
    recipe_book_json = [
        jsonify_recipe(recipe)
        for recipe in recipe_book
    ]

    # The concrete map is simply the map template with the "I" replaced with
    # "-"
    concrete_map = [
        line.replace("I", "-")
        for line in map_template
    ]

    # The concrete items are the initial leaf items
    map_json = {
        "map": concrete_map,
        "items": concrete_items,
        "recipes": recipe_book_json,
    }


    # Transform the json into a map by using the map parser
    with tempfile.TemporaryDirectory() as tdir:
        map_path = Path(tdir)/"map.json"
        with open(map_path, "wt") as fp:
            json.dump(map_json, fp)
        map_spec = get_map_spec(map_path)

    return map_spec
