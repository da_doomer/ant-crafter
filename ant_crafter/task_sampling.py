"""Script to sample tasks."""
from ant_crafter.item_sequence_sampling import get_item_sequence
from ant_crafter.map_instantiation import get_map_instance
from ant_crafter.recipe_sampling import get_recipe_book
from ant_crafter.mujoco.map import MapSpec
from ant_crafter.mujoco.simulation import get_simulation_instance
from pathlib import Path
from dataclasses import dataclass
import concurrent.futures
import jsonpickle
import random
import json
import numpy as np
import argparse


map_template_dir = Path(__file__).parent/"map_templates"


@dataclass
class Task:
    map_spec: MapSpec
    item_sequence: list[str]

    def save_to_path(self, path: Path):
        with open(path, "wt") as fp:
            task_str = str(jsonpickle.encode(self, indent=2))
            fp.write(task_str)

    @staticmethod
    def from_path(path: Path) -> "Task":
        with open(path, "rt") as fp:
            task_str = fp.read()
        task: Task = jsonpickle.decode(task_str)
        return task


def sample_task(
    item_sequence_size: int,
    map_template_paths : list[Path],
    seed: str,
) -> Task:
    """Sample a random task using the built-in templates.

    - `item_sequence_size` is the number of target items.
    - `map_template_paths` is a list of JSON file paths.
    - `seed` is the random seed.
    """
    _random = random.Random(seed)

    # Get recipe book
    recipe_book = get_recipe_book(
        seed=str(_random.random()),
    )

    # Get map template
    map_template_path = _random.choice(map_template_paths)
    with open(map_template_path, "rt") as fp:
        map_template = json.load(fp)["map"]

    # Get item sequence
    item_sequence = get_item_sequence(
        recipe_book=recipe_book,
        size=item_sequence_size,
        seed=str(_random.random()),
    )

    # Assemble map instance
    map_spec = get_map_instance(
        recipe_book=recipe_book,
        map_template=map_template,
        item_sequence=item_sequence,
        seed=str(_random.random()),
    )

    # Assemble task
    task = Task(
        map_spec=map_spec,
        item_sequence=item_sequence,
    )
    return task


def sample_task_to_dir(
    item_sequence_size: int,
    map_template_paths: list[Path],
    output_dir: Path,
    seed: str,
):
    """The `output_dir` is assumed to exist. And

    - `map_template_paths` is a list of JSON file paths.
    """
    task = sample_task(
        item_sequence_size=item_sequence_size,
        map_template_paths=map_template_paths,
        seed=seed,
    )

    # Serialize task
    task_path = output_dir/"task.json"
    task.save_to_path(task_path)
    print(f"Wrote {task_path}")

    # Save a short video
    video_step_n = 180
    sim = get_simulation_instance(
        map_spec=task.map_spec,
        animation_fps=60.0,
        sub_step_s=1.0/10,
    )
    for _ in range(video_step_n):
        action = np.zeros((task.map_spec.robot.actuator_n))
        sim.step(action)
    video_output = output_dir/"video.mp4"
    sim.save_video(video_output)
    print(f"Wrote {video_output}")


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        prog="Task generator",
        description='Generate tasks and write them to a directory.',
    )
    parser.add_argument(
        '--seed',
        type=str,
        required=True,
        help='Random seed.',
    )
    parser.add_argument(
        '--task_n',
        type=int,
        required=True,
        help='Number of tasks to generate.',
    )
    parser.add_argument(
        '--item_sequence_size',
        type=int,
        required=True,
        help='Number of tasks to generate.',
    )
    parser.add_argument(
        '--map_templates',
        type=Path,
        required=True,
        nargs='+',
        help='Paths to the JSON map templates that are to be used.',
    )
    parser.add_argument(
        '--output_dir',
        type=Path,
        required=True,
        help='Output directory, assumed not to exist.',
    )
    args = parser.parse_args()
    _random = random.Random(args.seed)

    output_dir = args.output_dir
    output_dir.mkdir()

    for path in args.map_templates:
        error_message = f"Map template '{path}' not found!"
        assert path.exists(), error_message

    task_dirs = list()

    max_workers = 3
    with concurrent.futures.ThreadPoolExecutor(max_workers) as pool:
        futures = list()
        for i in range(args.task_n):
            task_dir = output_dir/f"task_{i}"
            task_dir.mkdir()
            #future = pool.submit(
                #sample_task_to_dir,
            sample_task_to_dir(
                item_sequence_size=args.item_sequence_size,
                output_dir=task_dir,
                map_template_paths=args.map_templates,
                seed=str(_random.random()),
            )
            #futures.append(future)
            task_dirs.append(task_dir.name)

        for future in futures:
            future.result()

    task_dirs_json = output_dir/"task_dirs.json"
    with open(task_dirs_json, "wt") as fp:
        json.dump(task_dirs, fp, indent=2)
    print(f"Wrote {task_dirs_json}")
