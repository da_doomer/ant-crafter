"""A map parser that turns text files into
`ant_crafter.mujoco.map.MapSpec`s.

These maps are organized into a grid for ease of description. Here is an example
with a crafting table, a door, a button and walls.

```json
{
	"map": [
		"--F--",
		"--R--",
		"-----",
		"-P-E-"
	],
	"items": [
		{ "item_name": "apple", "position": [0, 2] }
	]
}
```

The file sections cannot be reordered (i.e., "MAP:" should always be before
"ITEMS:").

- `W` stands for "wall"
- `R` stands for the initial position of the robot
- `T` stands for crafting table
- `K` stands for a key
- `X` stands for a trashcan
- `E` stands for a dining table
- `F` stands for a frying pan
- `D` stands for a door
- `B` stands for a button
- `P` stands for a plate dispenser
- `-` stands for empty space

The positions are positions in the map. The position on the bottom left is
`(0, 0)`, and increases horizontally on the first coordinate, and vertically
on the second coordinate.

Item names are names of the models in
[github.com/vikashplus/object_sim](https://github.com/vikashplus/object_sim).
"""
from pathlib import Path
from ant_crafter.mujoco.map import ButtonSpec
from ant_crafter.mujoco.map import CraftingTableSpec
from ant_crafter.mujoco.map import DoorSpec
from ant_crafter.mujoco.map import KeySpec
from ant_crafter.mujoco.map import MapSpec
from ant_crafter.mujoco.map import Position
from ant_crafter.mujoco.map import RobotSpec
from ant_crafter.mujoco.map import WallSpec
from ant_crafter.mujoco.map import TrashcanSpec
from ant_crafter.mujoco.map import DiningTableSpec
from ant_crafter.mujoco.map import FryingPanSpec
from ant_crafter.mujoco.map import PlateDispenserSpec
from ant_crafter.mujoco.map import ItemSpec
from ant_crafter.mujoco.map import Recipe
from ant_crafter.mujoco.map import FryingRecipe
from ant_crafter.mujoco.simulation import BASE_LENGTH
import ant_crafter.mujoco.simulation as simulation
from itertools import product
from dataclasses import dataclass
import json


ROBOT_NUM_LEGS = 4


@dataclass
class PreParsedItem:
    item_name: str
    position: tuple[int, int]


@dataclass
class PreParsedMap:
    map_array: list[str]
    items: list[PreParsedItem]
    recipes: list[Recipe]
    frying_recipes: list[FryingRecipe]


def jsonify_recipe(recipe: Recipe) -> dict:
    """Return a JSON-serializable representation of the dataset."""
    json_recipe = dict(
        input_items=recipe.input_items,
        output_item=recipe.output_item,
    )
    return json_recipe


def get_pre_parsed_map(file: Path) -> PreParsedMap:
    """Parser of the raw map file into the sections contained in the file."""
    with open(file, "rt") as fp:
        map_json = json.load(fp)

    # Identify sections
    map_array = map_json["map"]

    # Parse map lines
    if not len(set(map(len, map_array))) == 1:
        raise ValueError("Map file has inconsistent row lengths!")
    if len(map_array) == 0 or len(map_array[0]) == 0:
        raise ValueError("Map file appears to be empty!")

    # Transpose so that we can use the first index as coordinate
    map_array = [
        "".join([
            map_array[i][j]
            for i in reversed(range(len(map_array)))
        ])
        for j in range(len(map_array[0]))
    ]

    # Identify items
    item_json = map_json["items"]
    items = [
        PreParsedItem(
            item_name=item["item_name"],
            position=tuple(item["position"])
        )
        for item in item_json
    ]

    # Parse recipes
    recipe_json = map_json["recipes"]
    recipes = [
        Recipe(
            input_items=recipe["input_items"],
            output_item=recipe["output_item"],
        )
        for recipe in recipe_json
    ]

    frying_recipe_json = map_json["frying_recipes"]
    frying_recipes = [
        FryingRecipe(
            input_item=recipe["input_item"],
            output_item=recipe["output_item"],
        )
        for recipe in frying_recipe_json
    ]

    pre_parsed_map = PreParsedMap(
        map_array=map_array,
        items=items,
        recipes=recipes,
        frying_recipes=frying_recipes,
    )
    return pre_parsed_map


def get_door_width_depth(map_array: list[str], i: int, j: int) -> tuple[float, float]:
    """Very, very hacky function that guesses the orientation of a door
    and returns the corresponding door width and depth."""
    LONG_SIDE = BASE_LENGTH/2
    SHORT_SIDE = BASE_LENGTH/5
    if i == 0 or i == len(map_array[0]):
        # Guess the door is facing up
        width = LONG_SIDE
        depth = SHORT_SIDE
    elif j == 0 or j == len(map_array):
        # Guess the door is facing sideways
        width = SHORT_SIDE
        depth = LONG_SIDE
    else:
        # Check if the door is surrounded by walls
        left = map_array[i-1][j]
        right = map_array[i+1][j]

        if left == "D" or right == "D":
            width = LONG_SIDE
            depth = SHORT_SIDE
        else:
            width = SHORT_SIDE
            depth = LONG_SIDE
    return width, depth


def get_map_spec(file: Path) -> MapSpec:
    """Parse the given file into a map specification."""
    pre_parsed_map = get_pre_parsed_map(file)
    map_array = pre_parsed_map.map_array

    x_coordinates = list(range(len(map_array)))
    y_coordinates = list(range(len(map_array[0])))

    # Parse the map
    robot: (None|RobotSpec) = None
    x_coordinates = list(range(len(map_array)))
    crafting_tables = list[CraftingTableSpec]()
    keys = list[KeySpec]()
    doors = list[DoorSpec]()
    buttons = list[ButtonSpec]()
    walls = list[WallSpec]()
    trashcans = list[TrashcanSpec]()
    dining_tables = list[DiningTableSpec]()
    frying_pans = list[FryingPanSpec]()
    plate_dispensers = list[PlateDispenserSpec]()
    offset = BASE_LENGTH/2
    positions = dict[tuple[int, int], Position]()
    for i, j in product(x_coordinates, y_coordinates):
        # Position of the center of the grid
        position = Position(i*BASE_LENGTH+offset, j*BASE_LENGTH+offset)
        char = map_array[i][j]

        # Keep track of the mapping between integer coordinates
        # and simulation coordinates
        positions[(i, j)] = position

        match char:
            case "R":
                robot = RobotSpec(
                    position=position,
                    num_legs=ROBOT_NUM_LEGS,
                )
            case "T":
                crafting_table = CraftingTableSpec(
                    position=position,
                )
                crafting_tables.append(crafting_table)
            case "K":
                key = KeySpec(position=position)
                keys.append(key)
            case "W":
                wall = WallSpec(
                    position=position,
                    width=BASE_LENGTH/4,
                    depth=BASE_LENGTH/2,
                    height=BASE_LENGTH/2,
                )
                walls.append(wall)
            case "X":
                can = TrashcanSpec(
                    position=position,
                    width=BASE_LENGTH/4,
                    depth=BASE_LENGTH/4,
                    height=BASE_LENGTH/20,
                )
                trashcans.append(can)
            case "E":
                table = DiningTableSpec(
                    position=position,
                    width=BASE_LENGTH/4,
                    depth=BASE_LENGTH/4,
                    height=BASE_LENGTH/20,
                )
                dining_tables.append(table)
            case "D":
                width, depth = get_door_width_depth(
                    map_array=map_array,
                    i=i,
                    j=j,
                )
                door = DoorSpec(
                    position=position,
                    width=width,
                    height=depth,
                    depth=BASE_LENGTH/2,
                )
                doors.append(door)
            case "B":
                button = ButtonSpec(position=position)
                buttons.append(button)
            case "P":
                plate = PlateDispenserSpec(
                    position=position,
                    width=BASE_LENGTH/4,
                    depth=BASE_LENGTH/4,
                    height=BASE_LENGTH/20,
                )
                plate_dispensers.append(plate)
            case "F":
                frying_pan = FryingPanSpec(
                    position=position,
                    width=BASE_LENGTH/4,
                    depth=BASE_LENGTH/4,
                    height=BASE_LENGTH/20,
                )
                frying_pans.append(frying_pan)
            case "-":
                pass
            case other:
                raise ValueError(f"Map character {other} not recognized!")

    if robot is None:
        raise ValueError("Robot not in map!")

    # Assemble items
    try:
        items = [
            ItemSpec(
                item_name=item.item_name,
                position=positions[item.position],
            )
            for item in pre_parsed_map.items
        ]
    except KeyError:
        raise ValueError("Item is outside the map!")

    # Define width and depth
    width = len(x_coordinates)*BASE_LENGTH
    depth = len(y_coordinates)*BASE_LENGTH

    map_spec = MapSpec(
        robot=robot,
        items=items,
        crafting_tables=crafting_tables,
        trashcans=trashcans,
        dining_tables=dining_tables,
        walls=walls,
        doors=doors,
        buttons=buttons,
        width=width,
        depth=depth,
        frying_pans=frying_pans,
        plate_dispensers=plate_dispensers,
        frying_recipes=pre_parsed_map.frying_recipes,
        recipes=pre_parsed_map.recipes,
    )
    return map_spec


if __name__ == "__main__":
    import argparse
    import numpy as np
    parser = argparse.ArgumentParser(
        description='Map parsing debugging script.',
    )
    parser.add_argument(
        '--map',
        type=Path,
        required=True,
        help='Input map file.',
    )
    parser.add_argument(
        '--output_dir',
        type=Path,
        required=True,
        help='Input map file.',
    )
    args = parser.parse_args()
    args.output_dir.mkdir()
    map_spec = get_map_spec(args.map)
    sim = simulation.get_simulation_instance(
        map_spec=map_spec,
        animation_fps=60.0,
        sub_step_s=0.3,
    )
    zero_action = np.zeros((map_spec.robot.actuator_n))
    for _ in range(10):
        sim.step(zero_action)
    output_path = args.output_dir/"video.mp4"
    sim.save_video(output_path)
    print(f"Wrote {output_path}")
