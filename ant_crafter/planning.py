"""Planning utilities."""
from ant_crafter.predicates import Predicate
from ant_crafter.mujoco.map import MapSpec
from ant_crafter.mujoco.simulation import SimulationState
from ant_crafter.robustness import get_robustness_value
from dataclasses import dataclass
import concurrent.futures
import cma
import numpy as np
import random
import time


@dataclass
class PlannerOutput:
    plan: list[list[float]]
    is_sat: bool


@dataclass
class PlannerOptions:
    initial_stdev: float
    popsize: int
    timeout_s: float


def get_plan(
    predicate: Predicate,
    map_spec: MapSpec,
    simulation_state: SimulationState,
    plan_len: int,
    max_iter_n: int,
    max_workers: int,
    sub_step_s: float,
    options: PlannerOptions,
    seed: str,
    verbose: bool,
    initial_candidate: list[list[float]] | None = None,
) -> PlannerOutput:
    """Get a plan that solves the given predicate."""
    if initial_candidate is None:
        x0 = np.zeros((plan_len, map_spec.robot.actuator_n))
    else:
        x0 = np.array(initial_candidate)

    _random = random.Random(seed)

    if max_iter_n <= 0:
        plan = PlannerOutput(
            plan=x0.tolist(),
            is_sat=False,
        )
        return plan

    size = plan_len * map_spec.robot.actuator_n
    es = cma.CMAEvolutionStrategy(
        x0.flatten(),
        options.initial_stdev,
        {
            "seed": int.from_bytes(_random.randbytes(4), "little"),
            "popsize": options.popsize,
            "BoundaryHandler": cma.s.ch.BoundTransform,
            "bounds": [size*[-1], size*[1]],
        }
    )
    start_t = time.time()
    iter_i = 0
    with concurrent.futures.ProcessPoolExecutor(max_workers) as pool:
        while True:
            iter_i += 1
            x = list(es.ask())
            if iter_i == 1:
                x.append(x0.flatten())
            futures = [
                pool.submit(
                    get_robustness_value,
                    plan=xi.reshape(x0.shape).tolist(),
                    map_spec=map_spec,
                    simulation_state=simulation_state,
                    predicate=predicate,
                    t=0,
                    sub_step_s=sub_step_s,
                )
                for xi in x
            ]
            values = list[float]()
            for future in futures:
                try:
                    value = -future.result()
                except Exception:
                    continue
                values.append(value)
            best_val = float(es.result[1])
            es.tell(x, values)

            # Check if termination condition is met
            elapsed_s = time.time() - start_t
            is_success = best_val < 0.0
            is_timeout = elapsed_s > options.timeout_s or iter_i > max_iter_n
            is_alg_exit = es.stop() is None
            terminate = any((
                is_success,
                is_timeout,
                is_alg_exit,
            ))
            if terminate:
                if verbose and is_success:
                    print("Planning success!")
                if verbose and is_timeout:
                    print("Planning timeout!")
                if verbose and is_alg_exit:
                    print("Algorithm non-success termination condition met!")
                break

            if verbose:
                _ = es.disp()

    best_x = np.array(es.result[0]).reshape(x0.shape).tolist()
    if es.result[1] > 0.0:
        is_sat = False
    else:
        is_sat = True
    planner_output = PlannerOutput(
        plan=best_x,
        is_sat=is_sat,
    )
    return planner_output
