# Ant-Crafter

An embodied crafting environment running on Mujoco.

**Task: Go to the third button**

![Video](example.webm)

## What's included

- Map description language
- Mujoco simulation
- Goal description with Signal Temporal Logic
- CMA-ES planner to find control inputs that satisfy goals
